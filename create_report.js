const reporter = require('cucumber-html-reporter');
const moment = require('moment');
const fs = require('fs');

const fileName = String(moment().format('YYYY-MM-DD_hh-mm-ss'));

const files = ['report', 'firefox_report', 'chrome_report'];
for (let file of files) {
    try {
        const options = {
            theme: 'bootstrap',
            jsonFile: file + '.json',
            output: `${fileName}-${file}.html`,
            reportSuiteAsScenarios: true,
            scenarioTimestamp: true,
            launchReport: true,
            screenshotsDirectory: 'screenshots/',
            storeScreenshots: true,
            noInlineScreenshots: true,
            metadata: {
                'Test Environment': 'STAGING',
                'Platform': 'Windows 11',
            }
        };
        reporter.generate(options);
        fs.unlinkSync(file + '.json');
    } catch (e) {
        if (e) console.log('Report can not be created');
    }
}