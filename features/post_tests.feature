@post
Feature: API tests - POST
  Swagger documentation should contain Schema with all restrictions in POST

  Scenario: POST note happy path
    When I send "default" payload
    Then I get response code 201
    And I can see response body with fields:
      | fieldName   |
      | title       |
      | description |
      | id          |

  Scenario Outline: POST note error cases
    Given I prepare "test" payload with below data:
      | title   | description   |
      | <title> | <description> |
    When I send "test" payload
    Then I get response code 422

    Examples:
      | title                                               | description                                         |
      |                                                     | some description                                    |
      | some title                                          |                                                     |
      |                                                     |                                                     |
      |                                                     | some description                                    |
      | some title                                          |                                                     |
      | 123456789012345678901234567890123456789012345678901 |                                                     |
      |                                                     | 123456789012345678901234567890123456789012345678901 |
      | 1                                                   |                                                     |
      |                                                     | 1                                                   |
      |                                                     |                                                     |
      #field is removed from payload
      | removed                                             | removed                                             |
      | some title                                          | removed                                             |
      | removed                                             | some description                                    |

