@get
Feature: API tests - GET
  !!!!!!!!There is no pagination in GET request when there is no ID send and all records are returned.

  Scenario: GET by ID
    Given I prepare "test" payload with below data:
      | title       | description |
      | randomTitle | testing que |
    And I send "test" payload
    And I get response code 201
    When I send GET request for "test" record
    Then I can see response body with fields and values:
      | fieldName   | fieldValue  |
      | title       | randomTitle |
      | description | testing que |

  Scenario: GET Search
    When I send GET request
    Then I get "firstNumber" of records
    And I send "default" payload
    And I get response code 201
    When I send GET request
    Then I get "secondNumber" of records
    And "secondNumber" of records is higher than "firstNumber" by 1

