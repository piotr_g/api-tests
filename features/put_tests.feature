@put
Feature: API tests - PUT
  !!!!!!!!PUT should never be used for update (it should be used by PATCH)

  Scenario: PUT note happy path
    Given I send "default" payload
    And I get response code 201
    When I send PUT into "default" record below data:
      | title     | description  |
      | New Title | testing desc |
    And I get response code 200
    Then I can see response body with fields and values:
      | fieldName   | fieldValue   |
      | title       | New Title    |
      | description | testing desc |

  Scenario Outline: PUT note error cases
    When I send PUT into "<ID>" record below data:
      | title     | description  |
      | New Title | testing desc |
    And I get response code 422
    Examples:
      | ID   |
      |      |
      | text |

  Scenario: PUT note error cases - deleted case
    When I send "default" payload
    And I send DELETE request for "default" record
    When I send PUT into "default" record below data:
      | title     | description   |
      | New Title | testing desc |
    And I get response code 404