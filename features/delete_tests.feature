@delete
Feature: API tests - DELETE

  Scenario: DELETE by ID
    Given I prepare "test" payload with below data:
      | title       | description |
      | randomTitle | testing que |
    And I send "test" payload
    And I get response code 201
    When I send DELETE request for "test" record
    Then I get response code 200

  Scenario Outline: DELETE by invalid ID
    When I send DELETE request for "<ID>>" record
    Then I get response code 422
    Examples:
      | ID   |
      |      |
      | text |


  Scenario: DELETE by already deleted ID
    Given I prepare "test" payload with below data:
      | title       | description |
      | randomTitle | testing que |
    And I send "test" payload
    And I get response code 201
    And I send DELETE request for "test" record
    And I get response code 200
    When I send DELETE request for "test" record
    Then I get response code 404