const {When, Then} = require("@cucumber/cucumber");
const {restGet} = require('../support/restHelpers');
const _ = require("lodash");
const assert = require('assert');
const {sharedData} = require('./commonSteps')
const path = sharedData.path

When(/^I send GET request for "([^"]*)" record$/, async function (payloadName) {
    const recordId = this.context[payloadName].body.id
    this.context.response = await restGet(`${path}${recordId}/`)
});

When(/^I can see response body with fields and values:$/, function (table) {
    const hashedTable = table.hashes();
    const responseBody = this.context.response.body;
    try {
        hashedTable.forEach(function (entry) {
            const responseFieldValue = _.get(responseBody, entry.fieldName).toString();
            const expectedFieldValue = entry.fieldValue;
            assert.equal(responseFieldValue, expectedFieldValue);
        });
    } catch (e) {
        throw new Error(e);
    }
});

When(/^I send GET request$/, async function () {
    this.context.response = await restGet(`${path}`)
});

Then(/^I get "([^"]*)" of records$/, function (alias) {
    this.context[alias] = this.context.response.body.length
});

Then(/^"([^"]*)" of records is higher than "([^"]*)" by (\d+)$/, function (alias1, alias2, number) {
    assert.equal(this.context[alias1] - this.context[alias2] - number, 0);

});