const tv4 = require('tv4');

const checkSchema = function (dataToCheck, schemaFileName) {
    const checkSchema = tv4.validate(dataToCheck, schemaFileName, false, false);
    if (!checkSchema) {
        throw new Error('Path : ' + tv4.error.dataPath + ' => ' + tv4.error.message);
    }
};
module.exports = {
    checkSchema
};