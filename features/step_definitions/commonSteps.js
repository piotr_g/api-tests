const {Given, When, Then, BeforeAll} = require('@cucumber/cucumber');
const _ = require('lodash');
const {restDelete, restPut} = require('../support/restHelpers');
const {checkSchema} = require('./schemaValidation');
const assert = require('assert');
const defaultSchema = require('../schemas/defaults.json');
const path = 'notes/'
let sharedData = {
    path
};

BeforeAll(function () {
    sharedData = {};
});

Then(/^I get response code (\d+)$/, async function (statusCode) {
    assert.equal(this.context.response.status, statusCode);
});

Then(/^I can see below fields:$/, function (table) {
    const hashedTable = table.hashes();
    const responseBody = this.context.response.body;
    try {
        hashedTable.forEach(function (entry) {
            const responseFieldValue = _.get(responseBody, entry.fieldName).toString();
            const expectedFieldValue = entry.fieldValue;
            assert.equal(responseFieldValue, expectedFieldValue);
        });
    } catch (e) {
        throw new Error(e);
    }

});

module.exports = {
    sharedData
}
When(/^I send DELETE request for "([^"]*)" record$/, async function (payloadName) {
    let recordId = 0
    if (this.context[payloadName] === undefined) recordId = payloadName;
    else if (payloadName && payloadName !== '') {
        recordId = this.context[payloadName].body.id;
    }
    this.context.response = await restDelete(`${path}${recordId}/`)
});
When(/^I send PUT into "([^"]*)" record below data:$/, async function (alias, table) {
    let recordId = 0
    if (this.context[alias] === undefined) recordId = alias;
    else if (alias && alias !== '') {
        recordId = this.context[alias].body.id;
    }
    const hashedTable = table.hashes();
    for (const element in hashedTable[0]) {
        if (hashedTable[0][element] === 'removed') delete hashedTable[0][element]
    }
    this.context.response = await restPut(`${path}${recordId}/`, hashedTable[0])
    this.context[alias] = hashedTable[0];
});