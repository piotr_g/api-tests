const {Given, When, Then} = require('@cucumber/cucumber');
const _ = require('lodash');
const {restPost} = require('../support/restHelpers');
const assert = require('assert');
const moment = require('moment');
const getTimestamp = () => String(moment().toDate().getTime());
const {sharedData} = require('./commonSteps')
const path = sharedData.path

const defaultPayload = {
    "title": "Tilte" + getTimestamp(),
    "description": "Some description"
}


When(/^I send "([^"]*)" payload$/, async function (payloadName) {
    let sendPayload = defaultPayload
    if (payloadName !== 'default') {
        sendPayload = this.context[payloadName]
    }
    const response = await restPost(path, sendPayload);
    this.context[payloadName] = response;
    this.context.response = response;
    if(response.body.id){
        if(!sharedData.idArray) sharedData.idArray = [];
        sharedData.idArray.push(response.body.id)
    }
});


Then(/^I can see response body with fields:$/, async function (table) {
    const hashedTable = table.hashes();
    const responseBody = this.context.response.body;
    try {
        hashedTable.forEach(function (entry) {
            const responseField = _.has(responseBody, entry.fieldName);

            assert.equal(responseField, true);
        });
    } catch (e) {
        throw new Error(e);
    }
});
Given(/^I prepare "([^"]*)" payload with below data:$/, function (alias, table) {
    const hashedTable = table.hashes();
    for (const element in hashedTable[0]) {
        if (hashedTable[0][element] === 'removed') delete hashedTable[0][element]
    }
    this.context[alias] = hashedTable[0];
});