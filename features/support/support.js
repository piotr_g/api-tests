'use script';
require('dotenv').config();
const {setWorldConstructor} = require('@cucumber/cucumber');

class CustomWorld {
    constructor({}) {
        this.context = {};
    }
}
setWorldConstructor(CustomWorld);

