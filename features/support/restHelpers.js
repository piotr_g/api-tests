'use strict';
const request = require('supertest');
const serverRoot = process.env.API_URL || 'http://127.0.0.1:8002/';

const restPost = async function (path, payload) {
    return await request(serverRoot).post(path).set('Content-Type', 'application/json; charset=utf-8').send(payload);
};

const restGet = async function (path) {
    return await request(serverRoot).get(path).set('Content-Type', 'application/json; charset=utf-8').send();
};

const restDelete = async function (path) {
    return await request(serverRoot).delete(path).send();
};

const restPut = async function (path, payload) {
    return await request(serverRoot).put(path).set('Content-Type', 'application/json; charset=utf-8').send(payload);
};

module.exports = {
    restPost,
    restGet,
    restDelete,
    restPut
};
