'use strict';
const {AfterAll} = require('@cucumber/cucumber');
const {sharedData} = require('../step_definitions/commonSteps')
const {restDelete} = require('./restHelpers');

//cleanup
AfterAll(async function () {
    sharedData.idArray.forEach(async function (entry) {
        await restDelete(`${sharedData.path}${entry}/`)
    })
});
