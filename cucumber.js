const cucumber = require('@cucumber/cucumber');
const defaultTimeouts = {
    short: 500,
    medium: 1000,
    long: 10000
};

const reportName = process.env.REPORT_NAME || `report.json`;
const tags = '';

const defaultOptions =
    ` --format json:${reportName} --format @cucumber/pretty-formatter --publish-quiet ${tags}`;

module.exports = {
    default: defaultOptions,
    cucumber,
    defaultTimeouts
};
